# Launch some jobs for discovery regions.
#
# Each submission uses NJOBS seeds counting up form OFFSET.
# Seeds must only be unique within each region, where results will be merged.
#
# See condor_disco.sub and condor_disco.sh for how arguments are used.
#
# Usage:
#
#     Set HISTFITTER_INSTALL_PATH, HISTFITTER_PATH, and JOBS_PATH.
#
#     source examples/condor_submit_disco.sh
#

# Fill me in!
HISTFITTER_INSTALL_PATH=/users2/ballaben/HistFitter_root6.18.04
HISTFITTER_PATH=/users2/ballaben/c1c1-ww-hf/unblinded_results/DiscoveryFit_checks/condor
JOBS_PATH=/users2/ballaben/c1c1-ww-hf/unblinded_results/DiscoveryFit_checks/condor

# Set an environment.
setupATLAS
#lsetup "root 6.20.06-x86_64-centos7-gcc8-opt"
lsetup "root 6.18.04-x86_64-centos7-gcc8-opt"
source ${HISTFITTER_INSTALL_PATH}/setup.sh

cd ${HISTFITTER_PATH}

# OffShell
condor_submit \
    ${JOBS_PATH}/condor_disco.sub \
    REGION=SRD_SF0J_80 \
    START=0 STOP=30 COUNT=30 \
    NTOYS=3000 \
    OFFSET=0 \
    NJOBS=1
